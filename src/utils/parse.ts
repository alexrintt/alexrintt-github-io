import { LayoutProps } from "../components/layout";

type GetLayoutPropsArgs = LayoutProps & {
  blogOwner: {
    login: string;
    bio?: string;
  };
};

export function getLayoutProps(props: GetLayoutPropsArgs): LayoutProps {
  return {
    ...props,
    user: {
      bio: props.blogOwner.bio ?? undefined,
      login: props.blogOwner.login!,
      url: `https://github.com/${props.blogOwner.login}`,
    },
  };
}
