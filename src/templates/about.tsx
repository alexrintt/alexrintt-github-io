import "../styles/markdown-body.scss";
import React from "react";
import Layout from "../components/layout";
import { graphql, PageProps, HeadFC } from "gatsby";
import { GatsbyImage, getImage, ImageDataLike } from "gatsby-plugin-image";
import Seo from "../components/seo";

export default function AboutPage(props: PageProps<Queries.AboutPageQuery>) {
  const {
    data: {
      blogOwner: {
        avatarUrlSharpOptimized,
        profileReadme: {
          childMarkdownRemark: { html: blogOwnerProfileReadmeHtml },
        },
        ...blogOwner
      },
    },
  } = props as any;

  return (
    <Layout
      user={{
        bio: blogOwner.bio ?? undefined,
        name: blogOwner.name ?? undefined,
      }}
    >
      <div className="w-full flex justify-center">
        <section className="flex flex-col items-center py-8 px-4 w-full flex-1 max-w-screen-md">
          <div className="w-full flex justify-center flex-col items-center">
            <GatsbyImage
              className="rounded-full"
              image={getImage(avatarUrlSharpOptimized as ImageDataLike)!}
              alt={blogOwner.name ?? blogOwner.login!}
            />
            <h1 className="pt-1">{blogOwner.name}</h1>
            <p className="opacity-50 pb-4">@{blogOwner.login}</p>
          </div>
          <div
            className="markdown-body md-body-full w-full max-w-screen-md"
            dangerouslySetInnerHTML={{ __html: blogOwnerProfileReadmeHtml }}
          ></div>
        </section>
      </div>
    </Layout>
  );
}

export const Head: HeadFC = () => <Seo />;

export const query = graphql`
  query AboutPage($ownerLogin: String!) {
    blogOwner: gitHubUser(login: { eq: $ownerLogin }) {
      ...BlogOwnerData
      avatarUrlSharpOptimized {
        ...AvatarUrlSharpOptimizedData
      }
    }
  }
`;
