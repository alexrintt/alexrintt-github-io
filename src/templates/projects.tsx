import React from "react";
import Layout from "../components/layout";
import { graphql, PageProps, HeadFC } from "gatsby";
import { GatsbyImage, getImage, ImageDataLike } from "gatsby-plugin-image";

import { removeProtocol } from "../utils/remove-protocol";
import Seo from "../components/seo";

export default function ProjectsPage(
  props: PageProps<Queries.ProjectsPageQuery>
) {
  const {
    data: {
      blogOwner,
      allGitHubRepository: { nodes: unsortedRepositories },
    },
  } = props;

  const isString = (_: any): boolean => typeof _ === `string`;

  const TOPIC_PREFIX = `ar`;
  const TOPIC_TAG_KEYS = [`cat`, `state`];

  const repositories = [...unsortedRepositories].sort((repoA, repoB) => {
    const a = new Date(
      (repoA.pushedAt ?? repoA.updatedAt ?? repoA.createdAt) as string
    ).getMilliseconds();
    const z = new Date(
      (repoA.pushedAt ?? repoA.updatedAt ?? repoA.createdAt) as string
    ).getMilliseconds();

    return z! - a!;
  });

  const slugToTitle = (slug: string | string[]) => {
    const parts = typeof slug === `string` ? slug.split(/[-_]/g) : slug;

    return parts
      .map((part) => part[0].toUpperCase() + part.substring(1).toLowerCase())
      .join(` `);
  };

  const categorySort = [
    `website`,
    `algorithms`,
    `flutter-app`,
    `dart-package`,
    `gatsby-plugin`,
    `canvas`,
    `api-docs`,
  ].reverse();

  const getTopicData = (topicName: string) => {
    const [prefix, key, ...value] = topicName.split(`-`);
    return {
      prefix,
      key,
      value: value,
    };
  };

  const hasTopicKeyValue = (topicName: string, groupBy?: string[]) => {
    const { prefix, key } = getTopicData(topicName);

    const tagsKeys = groupBy
      ? TOPIC_TAG_KEYS.filter((t) => groupBy.includes(t))
      : TOPIC_TAG_KEYS;

    return prefix === TOPIC_PREFIX && tagsKeys.includes(key);
  };

  const tags = Array.from(
    new Set(
      repositories
        .map(
          (repository) =>
            repository.topics
              ?.filter((t) => hasTopicKeyValue(t?.name ?? ``, [`cat`]))
              ?.map((t) => t?.name) ?? []
        )
        .reduce((previous, current) => [...previous, ...current], [])
        .filter(isString) as string[]
    )
  );

  const groupedRepositories = Array.from(tags)
    .map((currentTagName: string, index: number) => ({
      tagName: currentTagName,
      displayTagName: (() => {
        const { prefix, key, value } = getTopicData(currentTagName);

        const name = slugToTitle(value);

        return name;
      })(),
      orderByField: (() => {
        const { value } = getTopicData(currentTagName);

        const index = categorySort.indexOf(value.join(`-`));

        return index;
      })(),
      repositories: repositories.filter((repository) =>
        repository.topics?.some((topic) => topic?.name === currentTagName)
      ),
    }))
    .sort(({ orderByField: a }, { orderByField: z }) => z - a);

  return (
    <Layout
      user={{
        bio: blogOwner?.bio ?? undefined,
        name: blogOwner?.name ?? undefined,
      }}
    >
      <section className="flex justify-center w-full bg-s2">
        <div className="relative max-w-screen-md">
          {groupedRepositories.map(
            ({ repositories, displayTagName, tagName }) => {
              return (
                <div className="max-w-full" key={tagName}>
                  <h1 className="sticky top-0 right-0 left-0 z-50 py-2 font-variant bg-s2">
                    {displayTagName}
                  </h1>
                  <div className="flex flex-col w-full">
                    {repositories.map((repository) => {
                      const repoUrl = repository.url!;
                      const homepageUrl = repository.homepageUrl || repoUrl;

                      const searchForTopic = (name: string) =>
                        repository.topics?.some((t) => t?.name === name) ??
                        false;

                      const isContrib = searchForTopic(`ar-state-contrib`);
                      const isArchived = searchForTopic(`ar-state-archived`);

                      let meta: string | null = null;
                      let extraStyle: string | null = null;

                      type LabelInfo = {
                        text?: string;
                        backgroundColor?: string;
                        color?: string;
                      };

                      const highlights: LabelInfo[] = [];

                      if (isContrib) {
                        meta = `Contribution`;
                      }

                      if (isArchived) {
                        meta = `Archived`;
                        extraStyle = `line-through`;
                      }

                      if (repository.primaryLanguage?.name) {
                        highlights.push({
                          text: repository.primaryLanguage?.name,
                        });
                      }

                      return (
                        <div
                          key={repository.name}
                          className={`mb-8 max-w-full flex flex-col md:flex-row group rounded-lg w-full`}
                        >
                          <div>
                            <a
                              className="border-b-2 border-s4 block overflow-hidden rounded-lg w-52 h-52 remove-link-effect"
                              href={homepageUrl}
                            >
                              <GatsbyImage
                                alt={repository.name!}
                                className={`grayscale bg-transparent border-s4 border rounded-lg w-full h-full object-cover transition-all ${
                                  isArchived
                                    ? ""
                                    : "group-hover:scale-105 group-hover:grayscale-0"
                                }`}
                                image={
                                  getImage(
                                    repository.openGraphImageUrlSharpOptimized as ImageDataLike
                                  )!
                                }
                              />
                            </a>
                          </div>
                          <div className="flex-1 pt-4 md:pl-4">
                            <div className="flex flex-wrap">
                              {highlights.map((highlight) => {
                                return (
                                  <p
                                    key={highlight.text}
                                    className={`${highlight.backgroundColor} ${
                                      highlight.color
                                    } mb-2 mr-2 rounded-md text-sm py-1 px-2 border ${
                                      highlight.backgroundColor ?? "border-s4"
                                    }`}
                                  >
                                    {highlight.text}
                                  </p>
                                );
                              })}
                            </div>
                            <div className="flex justify-start overflow-hidden max-w-full">
                              <a
                                href={repoUrl}
                                className="block mb-2 tracking-tight w-fill-content break-all"
                              >
                                {removeProtocol(repoUrl)}
                              </a>
                            </div>
                            <div className="flex items-center max-w-full overflow-hidden">
                              <a
                                className={
                                  "mb-2 text-2xl font-bold font-variant" +
                                  (isArchived ? " " + (extraStyle ?? "") : "")
                                }
                                href={homepageUrl}
                              >
                                {slugToTitle(repository.name!)}
                              </a>
                              {meta && (
                                <sup className="block opacity-50 h-5">
                                  {meta}
                                </sup>
                              )}
                            </div>
                            {repository.description ? (
                              <p className="font-normal text-body">
                                {repository.description}
                              </p>
                            ) : (
                              <p className="font-normal text-body italic opacity-50">
                                No description provided
                              </p>
                            )}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            }
          )}
        </div>
      </section>
    </Layout>
  );
}

export const Head: HeadFC = () => <Seo />;

export const query = graphql`
  query ProjectsPage($ownerLogin: String!) {
    blogOwner: gitHubUser(login: { eq: $ownerLogin }) {
      ...BlogOwnerData
    }
    allGitHubRepository(
      filter: { topics: { elemMatch: { name: { regex: "/^ar-/g" } } } }
      sort: { fields: [pushedAt], order: [DESC] }
    ) {
      nodes {
        id
        name
        description
        homepageUrl
        url
        createdAt
        updatedAt
        pushedAt
        stargazerCount
        openGraphImageUrlSharpOptimized {
          ...ProjectCoverThumbnailImageData
        }
        topics {
          githubId
          name
        }
        primaryLanguage {
          id
          name
          color
        }
      }
    }
  }
`;
