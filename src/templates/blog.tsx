import * as React from "react";
import { graphql, Link, PageProps, HeadFC, useStaticQuery } from "gatsby";
import { GatsbyImage, getImage, ImageDataLike } from "gatsby-plugin-image";
import Pagination from "../components/pagination";
import Layout from "../components/layout";
import Seo from "../components/seo";

type BlogPostPreviewProps = {
  mini?: boolean;
  post: {
    title: string;
    excerpt: string;
    thumbnailImage?: ImageDataLike;
    humanReadableCreatedAt: string;
    subjects: string[];
    url: string;
    slug: string;
    timeToRead: string;
    author: {
      login: string;
      name: string;
      avatarUrlSharpOptimized: ImageDataLike;
    };
  };
  postsBasePath: string;
};

function BlogPostPreview({ post, mini, postsBasePath }: BlogPostPreviewProps) {
  return (
    <div className={`group my-8`}>
      <div className="max-w-full flex flex-col md:flex-row-reverse">
        {post.thumbnailImage && (
          <span
            className={`mb-2 block w-32 rounded-md overflow-hidden h-32 md:ml-4`}
          >
            <GatsbyImage
              className="group-hover:scale-105 transition-all w-full h-full object-cover"
              image={getImage(post.thumbnailImage as ImageDataLike)!}
              alt={post.title!}
            />
          </span>
        )}
        <span className="flex-4">
          <div className="flex flex-wrap">
            {post.subjects.length > 0 &&
              [...post.subjects].slice(0, 5).map((subject) => (
                <span key={subject} className="flex justify-start mr-1 mb-1">
                  <p className="font-variant text-p2">{subject}</p>
                </span>
              ))}
          </div>
          <span className="block">
            <Link
              to={postsBasePath + "/" + post.slug}
              className={`fancy-link text-p1 font-variant ${
                mini ? "text-2xl" : "text-3xl"
              }`}
            >
              {post.title}
            </Link>
          </span>
          <p className="text-body mt-1">{post.excerpt}</p>
          <div className="flex items-center pt-2">
            {
              <>
                <GatsbyImage
                  className="rounded-full"
                  image={
                    getImage(
                      post.author!.avatarUrlSharpOptimized as ImageDataLike
                    )!
                  }
                  alt={post.author!.name ?? post.author!.login!}
                />
                <span className="p-1"></span>
              </>
            }
            <div>
              <span className="block text-sm text-p2 font-variant">
                {post.author!.name ?? `@${post.author.login}`}
              </span>
              <span className="block text-sm text-p2 font-variant">
                {post.humanReadableCreatedAt}
              </span>
            </div>
          </div>
        </span>
      </div>
    </div>
  );
}

export type BlogPageContext = {
  currentPage: number;
  pageCount: number;
  skip: number;
  limit: number;
  ownerLogin: string;
  homePath: string;
  listingPath: string;
};

export default function BlogPage(
  props: PageProps<Queries.BlogPageQuery, BlogPageContext>
) {
  const {
    pageContext: { currentPage, pageCount, homePath, listingPath },
    data: {
      discussions: { nodes: discussions },
      blogOwner,
      site: {
        siteMetadata: { blogConfig },
      },
    },
  } = props;

  if (discussions.length <= 0) {
    return <></>;
  }

  function getSubjects(discussion: Queries.GitHubDiscussion): string[] {
    return (discussion.labels ?? [])
      .filter((l: any) => (l as Queries.GitHubLabel).name!.startsWith(`sub:`))
      .map((l) => l!.name!.substring("sub:".length));
  }

  return (
    <Layout
      user={{
        bio: blogOwner?.bio ?? undefined,
        name: blogOwner?.name ?? undefined,
      }}
    >
      <div className="w-full flex justify-center">
        <div className="w-full max-w-screen-md">
          <div className="flex">
            <section className="w-full">
              {discussions.map((discussion, i) => (
                <div key={discussion.githubId}>
                  <BlogPostPreview
                    mini
                    post={{
                      timeToRead:
                        discussion.childMarkdownRemark!.timeToRead!.toString(),
                      url: discussion.url!,
                      slug: discussion.slug!,
                      // mini={i % 8 !== 0}
                      subjects: getSubjects(
                        discussion as unknown as Queries.GitHubDiscussion
                      ),
                      excerpt: discussion.childMarkdownRemark!.longExcerpt!,
                      humanReadableCreatedAt:
                        discussion.humanReadableCreatedAt!,
                      thumbnailImage:
                        discussion.thumbnailImage as ImageDataLike,
                      title: discussion.title!,
                      author: {
                        avatarUrlSharpOptimized: discussion.author!
                          .avatarUrlSharpOptimized! as ImageDataLike,
                        login: discussion.author!.login!,
                        name: discussion.author!.name!,
                      },
                    }}
                    postsBasePath={blogConfig.postsBasePath}
                    key={discussion.slug}
                  />
                </div>
              ))}
            </section>
          </div>
        </div>
      </div>
      <span className="flex justify-center">
        <section className="max-w-screen-md px-2 py-8">
          <Pagination
            isActive={(page) => page !== currentPage}
            lastPage={pageCount}
            generateLink={(page) =>
              page === 1 ? homePath : `${listingPath}/${page}`
            }
          />
        </section>
      </span>
    </Layout>
  );
}

export const Head: HeadFC = () => <Seo />;

export const query = graphql`
  fragment BlogOwnerData on GitHubUser {
    login
    bio
    name
    profileReadme {
      childMarkdownRemark {
        html
        id
      }
    }
  }

  query BlogPage($ownerLogin: String!, $skip: Int!, $limit: Int!) {
    blogOwner: gitHubUser(login: { eq: $ownerLogin }) {
      ...BlogOwnerData
    }
    site {
      siteMetadata {
        blogConfig {
          postsBasePath
        }
      }
    }
    discussions: allGitHubDiscussion(
      # filter: { hasThumbnailImage: { eq: true } }
      sort: { fields: [createdAt, updatedAt], order: [DESC, DESC] }
      skip: $skip
      limit: $limit
    ) {
      nodes {
        category {
          githubId
          name
        }
        slug
        url
        author {
          name
          login
          avatarUrlSharpOptimized {
            ...AvatarUrlSharpOptimizedData
          }
        }
        labels {
          name
          githubId
        }
        thumbnailImage {
          ...PostCoverThumbnailImageData
        }
        childMarkdownRemark {
          longExcerpt: excerpt(truncate: true, pruneLength: 240)
          id
          timeToRead
        }
        ...PostPreviewInfo
      }
    }
  }
`;
