import "../styles/markdown-body.scss";

import * as React from "react";
import { graphql, HeadFC, navigate, PageProps } from "gatsby";
import { GatsbyImage, getImage, ImageDataLike } from "gatsby-plugin-image";

import { Disqus, CommentCount } from "gatsby-plugin-disqus";

import Layout from "../components/layout";
import Seo from "../components/seo";

type PostHeroProps = {
  heroImage: ImageDataLike;
  alt: string;
};

function PostHero(props: PostHeroProps) {
  return (
    <div className="flex justify-center">
      <div className="mb-8 flex max-w-screen-lg w-full justify-center relative overflow-hidden">
        <div className="w-full max-h-[75vh] max-w-full">
          <GatsbyImage
            className="border-s4 border transition-all w-full h-full"
            alt={props.alt}
            image={getImage(props.heroImage)!}
          />
        </div>
      </div>
    </div>
  );
}

type PostHeaderProps = {
  post: {
    slug: string;
    title: string;
    timeAgo: string;
    humanReadableCreatedAt: string;
    editPostUrl: string;
    author: {
      avatarUrlSharpOptimized: ImageDataLike;
      login: string;
      name?: string;
      url: string;
    };
  };
};

function PostHeader({ post }: PostHeaderProps) {
  const { author } = post;

  const avatarUrlSharpOptimized = getImage(post.author.avatarUrlSharpOptimized);

  return (
    <div className="flex items-center flex-col mt-8">
      <div className="container max-w-screen-md">
        <div className="flex flex-col items-start">
          <button
            className="flex as-link items-center text-sm as-link font-code"
            onClick={() => navigate(-1)}
          >
            cd ../
          </button>
          <a
            className="inline-block text-sm hover:opacity-100 mb-4 font-code mt-2"
            href={post.editPostUrl}
          >
            nano {post.slug}.md
          </a>
          <p className="text-p2 font-variant">{post.humanReadableCreatedAt}</p>
          <p className="text-p2 font-variant">{post.timeAgo}</p>
        </div>
        <div className={`markdown-body md-body-title md-body-post-title my-3`}>
          <span className="markdown-body md-body-title">
            <h1>{post.title}</h1>
          </span>
        </div>
        <div className="flex justify-start mb-8">
          <a className="group block remove-link-effect" href={post.author.url}>
            <div className="flex justify-start items-center">
              <GatsbyImage
                key={post.author.url}
                alt={post.author.name ?? post.author.login}
                image={avatarUrlSharpOptimized!}
                className="rounded-full border border-s4"
              />
              <span className="pl-2 text-sm text-p1 whitespace-nowrap">
                {author.name && (
                  <>
                    <span className="font-variant">{author.name}</span>
                    <br />
                  </>
                )}
                <span className="font-variant whitespace-nowrap relative before:content-['@'] before:opacity-50 before:text-p2 text-p2">
                  {author.login}
                </span>
              </span>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}

export default function BlogPostPage(
  props: PageProps<Queries.BlogPostPageQuery>
) {
  const {
    data: { gitHubDiscussion, blogOwner },
  } = props;

  const post = gitHubDiscussion!;

  const thumbnailImage = getImage(post?.thumbnailImage as ImageDataLike);

  return (
    <Layout
      user={{
        bio: blogOwner?.bio ?? undefined,
        name: blogOwner?.name ?? undefined,
      }}
    >
      <section className="post-info">
        <PostHeader
          post={{
            slug: post.slug!,
            editPostUrl: post.editPostUrl!,
            humanReadableCreatedAt: post.humanReadableCreatedAt!,
            timeAgo: post.timeAgo!,
            title: post.title!,
            author: {
              avatarUrlSharpOptimized: post.author!
                .avatarUrlSharpOptimized as ImageDataLike,
              login: post.author!.login!,
              url: post.author!.url!,
              name: post.author!.name ?? undefined,
            },
          }}
        />
        {thumbnailImage && (
          <PostHero
            alt={post.title!}
            heroImage={post.thumbnailImage as ImageDataLike}
          />
        )}
      </section>
      <div className="w-full flex justify-center">
        <div className="w-full max-w-screen-md">
          <div
            className="markdown-body md-body-full"
            dangerouslySetInnerHTML={{
              __html: post?.childMarkdownRemark?.html ?? ``,
            }}
          ></div>
        </div>
      </div>
      <div className="flex justify-center py-10">
        <div className="max-w-screen-md flex-1">
          <Disqus
            config={{
              url: post.url,
              identifier: post.githubId,
              title: post.title,
            }}
          />
        </div>
      </div>
    </Layout>
  );
}

export const Head: HeadFC<Queries.BlogPostPageQuery> = (props) => {
  return (
    <Seo
      title={props.data.gitHubDiscussion?.title ?? undefined}
      description={
        props.data.gitHubDiscussion?.childMarkdownRemark?.shortExcerpt ??
        undefined
      }
      image={
        props.data.gitHubDiscussion?.thumbnailImage?.publicURL ?? undefined
      }
    />
  );
};

export const query = graphql`
  fragment PostPreviewInfo on GitHubDiscussion {
    slug
    title
    githubId
    url
    editPostUrl: discussionUrl
    humanReadableCreatedAt: createdAt(formatString: "dddd, MMMM Do YYYY")
    timeAgo: createdAt(fromNow: true)
  }
  fragment AvatarUrlSharpOptimizedData on File {
    childImageSharp {
      gatsbyImageData(
        width: 35
        height: 35
        placeholder: BLURRED
        layout: FIXED
        formats: [AUTO, WEBP, AVIF]
      )
    }
  }

  fragment PostCoverImageData on File {
    childImageSharp {
      gatsbyImageData(
        width: 1920
        placeholder: BLURRED
        formats: [AUTO, WEBP, AVIF]
      )
    }
  }

  fragment PostCoverThumbnailImageData on File {
    childImageSharp {
      gatsbyImageData(
        width: 150
        height: 150
        layout: FIXED
        placeholder: BLURRED
        formats: [AUTO, WEBP, AVIF]
      )
    }
  }

  fragment ProjectCoverThumbnailImageData on File {
    childImageSharp {
      gatsbyImageData(
        width: 250
        height: 250
        layout: FIXED
        placeholder: BLURRED
        formats: [AUTO, WEBP, AVIF]
      )
    }
  }

  query BlogPostPage($discussionGithubId: String!, $ownerLogin: String!) {
    blogOwner: gitHubUser(login: { eq: $ownerLogin }) {
      ...BlogOwnerData
    }
    gitHubDiscussion(githubId: { eq: $discussionGithubId }) {
      category {
        githubId
        name
      }
      childMarkdownRemark {
        html
        id
      }
      author {
        login
        name
        url
        avatarUrlSharpOptimized {
          ...AvatarUrlSharpOptimizedData
        }
      }
      thumbnailImage {
        ...PostCoverImageData
        publicURL
      }
      ...PostPreviewInfo
      childMarkdownRemark {
        shortExcerpt: excerpt(truncate: true, pruneLength: 120)
      }
    }
  }
`;
