import React from "react";

export type FooterItem = {
  link: string;
  text: string;
};

export type FooterLink = {
  title: string;
  items: FooterItem[];
};

export type FooterProps = {
  links: FooterLink[];
  title: string;
};

export default function Footer({ links, title }: FooterProps): JSX.Element {
  const menus = Array.from(links).sort(
    (a, b) => b.items.length - a.items.length
  );

  type TitleProps = {
    fontSize: string;
  };

  const Title: React.FC<TitleProps> = ({ fontSize }) => (
    <>
      {title.split(".").map((part, i, arr) => {
        const isLast = i === arr.length - 1;

        return (
          <p
            key={i}
            className={`${fontSize} whitespace-nowrap font-variant ${
              isLast ? "opacity-100" : "opacity-50"
            } ${isLast ? `text-p1` : `text-p2`}`}
          >
            {part}
            {isLast ? `` : `.`}
          </p>
        );
      })}
    </>
  );

  return (
    <>
      <div className="force-dark max-w-full flex justify-center py-4">
        <div className="max-w-screen-md w-full">
          <div className="py-5 pb-10 flex">
            <Title fontSize="text-lg" />
          </div>
          <div className="w-full">
            <div className="flex flex-wrap">
              {menus.map((menu) => (
                <div key={menu.title} className="text-p1 mr-8 mb-8">
                  <p className="font-variant text-xl mb-5">{menu.title}</p>
                  {menu.items.map((item) => (
                    <div key={item.text} className="mb-2">
                      <a href={item.link} className="text-p2 hover:text-p1">
                        {item.text}
                      </a>
                    </div>
                  ))}
                </div>
              ))}
            </div>
            <div className="flex justify-center py-8">
              <div
                className="whitespace-nowrap p-2 p-4 flex before:content-['@'] before:text-p2 before:opacity-25 font-variant text-p2"
                title={title}
              >
                2020 - {new Date(Date.now()).getFullYear()}{" "}
                <span className="p-1"></span>
                <Title fontSize="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
