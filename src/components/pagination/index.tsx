import { Link } from "gatsby";
import React from "react";

export type PaginationProps = {
  isActive: (page: number, index: number) => boolean;
  lastPage: number;
  generateLink: (page: number, index: number) => string;
  generateText?: (page: number, index: number) => string;
};

export default function Pagination(props: PaginationProps) {
  const defaultTextGenerator = (page: number, index: number) => `${page}`;
  const textGenerator = props.generateText ?? defaultTextGenerator;

  return (
    <div className="flex justify-center flex-wrap">
      {Array.from({ length: props.lastPage }).map((_, index) => (
        <Link
          key={index}
          aria-disabled={!props.isActive(index + 1, index)}
          className={`hover:underline ${
            props.isActive(index + 1, index) ? "" : "pointer-events-none"
          } font-variant text-md text-link block p-2 ${
            props.isActive(index + 1, index) ? `` : `opacity-50`
          }`}
          to={props.generateLink(index + 1, index)}
        >
          {textGenerator(index + 1, index)}
        </Link>
      ))}
    </div>
  );
}
