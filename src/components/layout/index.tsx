import React from "react";
import { Link } from "@reach/router";
import Footer from "../footer/index";
import { useSiteMetadata } from "../../hooks/use-site-metadata";
import { graphql, useStaticQuery } from "gatsby";

export type LayoutProps = {
  user?: {
    login?: string;
    name?: string;
    bio?: string;
    url?: string;
  };
  header?: {
    text?: string;
    url?: string;
  };
  footer?: {
    links?: any[];
    title?: string;
  };
  options?: {
    background?: string;
  };
};

export default function Layout({
  user,
  header,
  footer,
  children,
  options,
}: React.PropsWithChildren<LayoutProps>) {
  const {
    site: {
      siteMetadata: { blogConfig },
    },
  } = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          blogConfig {
            owner
            postListBasePath
            createPostUrl
            basePath
            appId
            footer {
              title
              items {
                text
                link
              }
            }
          }
        }
      }
    }
  `);

  return (
    <div
      className={"w-full max-w-full px-4 " + (options?.background ?? "bg-s2")}
    >
      <header className="flex justify-center pt-8">
        <div className="flex justify-between w-full max-w-screen-md">
          <div className="group">
            <Link
              className="remove-link-effect"
              to={blogConfig.postListBasePath}
            >
              <h1 className="font-variant text-2xl text-p1 before:opacity-50">
                {user?.name ?? user?.login ?? blogConfig.owner}
              </h1>
              {user?.bio && (
                <p className="text-p1 opacity-50 font-variant">{user.bio}</p>
              )}
            </Link>
          </div>
          <div className="flex items-center">
            <a
              className="font-variant text-body flex items-center"
              href={header?.url ?? blogConfig.createPostUrl}
            >
              {header?.text ?? "Write a post"}
            </a>
          </div>
        </div>
      </header>
      <span className="max-w-full w-full flex block justify-center py-4">
        <nav className="flex justify-between">
          <div className="flex justify-between w-full flex-wrap">
            {[
              [`Projects`, `/projects`],
              [`Blog`, `/`],
              [`About`, `/about`],
            ].map(([text, path]) => (
              <Link
                key={text + path}
                className="m-2 flex-1 text-center font-variant"
                to={`${blogConfig.basePath}${path}`}
              >
                {text}
              </Link>
            ))}
          </div>
        </nav>
      </span>
      <section className="w-full max-w-full flex justify-center">
        <main className="flex-1 max-w-full">{children}</main>
      </section>
      <Footer
        links={footer?.links ?? blogConfig.footer}
        title={footer?.title ?? blogConfig.appId}
      />
    </div>
  );
}
