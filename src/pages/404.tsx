import * as React from "react";
import { Link, HeadFC } from "gatsby";
import Seo from "../components/seo";

const NotFoundPage = () => {
  return (
    <main className="h-screen w-screen flex justify-center items-center p-4">
      <div>
        <p className="text-p2 text-center max-w-screen-sm">
          Well, there's nothing here, except a blank screen with a meaningless
          text I wrote because I was bored. But just to let you know I probably
          created this vacuum space by forgetting to edit some string or remove
          some conditional, my condolences.{" "}
          <a href="/">You better go home, back to safety.</a>
        </p>
      </div>
    </main>
  );
};

export const Head: HeadFC = () => <Seo title="You found me" />;

export default NotFoundPage;
