import path from "path";
import { slugify } from "./src/utils/slugify";
import blogConfig from "./blog.config";
import type { GatsbyNode } from "gatsby";

type CreatePagesAPI = GatsbyNode[`createPages`];
type OnCreatePageAPI = GatsbyNode[`onCreatePage`];

const createBlogPagination: CreatePagesAPI = async ({ actions, graphql }) => {
  const { createPage } = actions;

  const query = `
    query GetAllGitHubDiscussions {
      allGitHubDiscussion {
        totalCount
      }
    }
  `;

  const response = await graphql<Queries.Query>(query);

  const data = response.data;

  if (!data) {
    throw Error(
      `Was not possible to fetch data: ` +
        data +
        "\nQuery: " +
        query +
        "\nErrors: " +
        response.errors
    );
  }

  const { totalCount } = data.allGitHubDiscussion;

  const perPage = blogConfig.postsPerPage;

  const pageCount = Math.ceil(totalCount / perPage);

  for (let i = 0; i < pageCount; i++) {
    const skip = i * perPage;
    const limit = perPage;
    const page = i + 1;

    const listingPath = `${blogConfig.postListBasePath}/page`;

    // Base path for post list (www.domain.com/)
    const homePath = blogConfig.postListBasePath || `/`;

    // If someday I want to realocate the post list to the post base path (www.domain.com/blog):
    // const homePath = blogConfig.postsBasePath;

    createPage({
      path: page === 1 ? homePath : `${listingPath}/${page}`,
      component: path.resolve(`./src/templates/blog.tsx`),
      context: {
        currentPage: page,
        pageCount: pageCount,
        homePath: homePath,
        listingPath: listingPath,
        ownerLogin: blogConfig.owner,
        skip,
        limit,
      },
    });
  }
};

const createAboutPage: CreatePagesAPI = async ({ actions, graphql }) => {
  const { createPage } = actions;

  createPage({
    path: `${blogConfig.basePath}/about`,
    component: path.resolve(`./src/templates/about.tsx`),
    context: {
      ownerLogin: blogConfig.owner,
    },
  });
};

const createProjectsPage: CreatePagesAPI = async ({ actions, graphql }) => {
  const { createPage } = actions;

  createPage({
    path: `${blogConfig.basePath}/projects`,
    component: path.resolve(`./src/templates/projects.tsx`),
    context: {
      ownerLogin: blogConfig.owner,
    },
  });
};

const createBlogPostPages: CreatePagesAPI = async ({ actions, graphql }) => {
  const { createPage } = actions;

  const query = `
    query GetAllGitHubDiscussions {
      allGitHubDiscussion {
        nodes {
          id
          githubId
          path
        }
      }
    }
  `;

  const response = await graphql<Queries.Query>(query);

  const data = response.data;

  if (!data) {
    throw Error(
      `Was not possible to fetch data: ` +
        data +
        "\nQuery: " +
        query +
        "\nErrors: " +
        response.errors
    );
  }

  for (const discussion of data.allGitHubDiscussion.nodes) {
    createPage({
      path: discussion.path!,
      component: path.resolve(`./src/templates/blog-post.tsx`),
      ownerNodeId: discussion.id,
      context: {
        discussionGithubId: discussion.githubId,
        ownerLogin: blogConfig.owner,
      },
    });
  }
};

export const createPages: CreatePagesAPI = async function (...args) {
  await createProjectsPage(...args);
  await createAboutPage(...args);
  await createBlogPagination(...args);
  await createBlogPostPages(...args);
};
