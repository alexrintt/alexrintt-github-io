## Repository Anatomy

- `blog` the actual blog website source code.
- `notes` some notes that appears in the production website (some kind of cms, though not implemented yet).
- `legacy` legacy code from previous versions [2018, 2019, ..., current year[.
- `specs` some boring rules I need to remember when creating new projects.

## CMS

- To submit a [new programming post](https://github.com/alexrinttln/programming/discussions/new?welcome_text=1&category=Submitted).
- To submit a [new philosophy post](https://github.com/alexrinttln/phisosophy/discussions/new?welcome_text=1&category=Submitted).

<br>

<samp>
  <p align="start">
    Website <a href="https://alexrintt.github.io">alexrintt.io</a>.
  </p>
</samp>
