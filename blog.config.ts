const getRepoUrl = (owner: string, name: string) =>
  `https://github.com/${owner}/${name}`;

const getRepoGitHubPagesUrl = (
  owner: string,
  name: string,
  customDomain?: string
) => `https://${customDomain ?? `${owner}.github.io`}/${name}`;

function getRepoDataFromOwnerAndName(...ownerAndRepoList: any[]) {
  type CompleteRepoData = {
    owner: string;
    name: string;
    url: string;
    githubPagesUrl: string;
  };

  return ownerAndRepoList.map((e) => {
    let owner = null,
      repo = null;

    if (typeof e === `string`) {
      const [sourceOwner, sourceRepo] = (e as string).split(`/`);
      owner = sourceOwner;
      repo = sourceRepo;
    } else if (typeof e === `object`) {
      owner = e.owner;
      repo = e.name;
    }

    return {
      owner,
      name: repo,
      url: getRepoUrl(owner, repo),
      githubPagesUrl: getRepoGitHubPagesUrl(owner, repo),
    };
  }) as CompleteRepoData[];
}

const config = {
  header: `Alex Rintt`,

  cmsRepositories: getRepoDataFromOwnerAndName(
    {
      owner: `alexrinttln`,
      name: `programming`,
    },
    {
      owner: `alexrinttln`,
      name: `philosophy`,
    }
  ),
  developmentCmsRepositories: getRepoDataFromOwnerAndName(
    {
      owner: `alexrinttln`,
      name: `programming-dev`,
    },
    {
      owner: `alexrinttln`,
      name: `programming`,
    },
    {
      owner: `alexrinttln`,
      name: `philosophy`,
    }
  ),
  websiteSourceRepository: getRepoDataFromOwnerAndName({
    owner: `alexrintt`,
    name: `alexrintt.github.io`,
  })[0],

  /**
   * To enable comments on your posts through Disqus you should provide your shortname in this option.
   *
   * To get your shortname you must create an app/site in the Disqus website then they will provide will this kind of API key.
   *
   * If do not want to enable comments, set this option to `null`.
   */
  disqusShortname: `rintt`,

  owner: `alexrintt`,

  /**
   * Slugs to include in the data-source, if not provided (null) all discussions will be fetched without any filter.
   * That means, if null, anyone can publish to your blog using a non-protected category.
   */
  discussionCategories: [`Published`],

  /**
   * Global blog base path if any (e.g: '/myblog'). Set to [null] to use '/'.
   */
  basePath: null,

  /**
   * WARNING: DO NOT EDIT IT AFTER YOU BLOG GOES TO PRODUCTION
   * OTHERWISE YOUR OLD POSTS URLS WILL NO LONGER BE AVAILABLE
   *
   * EDIT IT WHEN DEPLOYING YOUR BLOG FOR THE FIRST TIME OR
   * WHEN YOU DON'T CARE ABOUT LOSING YOUR OLD URLS
   *
   * Slug is the human readable ID of each post, but GitHub API has the limitation that
   * we can't offer a trusted slug system that will prevent you to create duplicated posts (same title).
   *
   * So to avoid it and keep readable, we can add extra-metadata to
   * the generated slug like {DATE} and {TIME} to make it even more unique (and thus prevent duplication).
   *
   * Available metadatas:
   * - {TITLE} Required, the title of the post (The title is transformed by `slugify` npm package).
   * - {DATE} in YYYY-MM-DD format.
   * - {TIME} in HH-MM format.
   *
   * Notes for those who are using [DATE] or [TIME] metadata:
   * - Due different time-zones some posts can create a slug in a 'future' or 'past' time.
   * - e.g: If you are somewhere GMT-3, since GitHub create issues using UTC time, your posts will be 3 hours in the future (since UTC is GMT-0).
   * - The displayed date in the browser will not be affected (it's translated to local time depending on client location).
   */
  slugPattern: `{TITLE}-{DATE}`,

  /**
   * Your posts will be shown at [mydomain.com/blog/myblog-post-title-123-abc].
   * Set to null if you want to omit the basepath.
   */
  postsPath: `/blog`,

  /**
   * Styling purposes.
   */
  appId: `io.alexrintt.blog`,

  /**
   * Base path for post list page, set to [null] to use `/`.
   */
  postListBasePath: `/`,

  /**
   * Base URL for all blog content.
   *
   * Use `https://yourusername.github.io/your-repo-name` if you're using a common repository.
   *
   * If you are using `https://github.com/yourusername/yourusername.github.io`
   * then you want to use `https://yourusername.github.io/` (without the path).
   */
  domain: `https://alexrintt.io`,

  /**
   * How many posts are shown in the homepage and subsequent pages (page 2, 3, 4...).
   */
  postsPerPage: 10,

  /**
   * Footer links.
   */
  footer: [
    {
      title: `Techs`,
      items: [
        {
          text: `React - UI Web Framework`,
          link: `https://reactjs.org/`,
        },
        {
          text: `GitHub - API and hosting`,
          link: `https://github.com/`,
        },
        {
          text: `Gatsby - JavaScript Framework`,
          link: `https://www.gatsbyjs.com/`,
        },
        {
          text: `Tailwind - CSS Framework`,
          link: `https://tailwindcss.com/`,
        },
      ],
    },
    {
      title: `Fonts`,
      items: [
        {
          text: `Source Sans Pro`,
          link: `https://fonts.google.com/specimen/Source+Sans+Pro`,
        },
        {
          text: `Source Serif Pro`,
          link: `https://fonts.google.com/specimen/Source+Serif+Pro`,
        },
      ],
    },
  ],
};

const createPostUrl = `${getRepoUrl(
  config.cmsRepositories[0].owner,
  config.cmsRepositories[0].name
)}/discussions/new?welcome_text=true&category=Submitted`;

const footerAboutSection = [
  {
    title: `About`,
    items: [
      {
        text: `GitHub Repository`,
        link: config.websiteSourceRepository.url,
      },
      {
        text: `Create a post`,
        link: createPostUrl,
      },
      {
        text: `Submit an Issue`,
        link: `${config.websiteSourceRepository.url}/issues`,
      },
      {
        text: `Pull Requests`,
        link: `${config.websiteSourceRepository.url}/pulls`,
      },
      {
        text: `Contributors`,
        link: `${config.websiteSourceRepository.url}/graphs/contributors`,
      },
    ],
  },
];

const basePath =
  config.basePath === `/` || typeof config.basePath !== `string`
    ? ``
    : config.basePath;

const postsBasePath = basePath + (config.postsPath ?? ``);

const postListBasePath =
  basePath +
  (config.postListBasePath === `/` ||
  typeof config.postListBasePath !== `string`
    ? ``
    : config.postListBasePath);

export default {
  ...config,
  createPostUrl,
  footer: [...footerAboutSection, ...config.footer],
  basePath,
  postListBasePath,
  postsBasePath,
};
