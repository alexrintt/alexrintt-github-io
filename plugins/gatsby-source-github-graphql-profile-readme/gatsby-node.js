exports.pluginOptionsSchema = ({ Joi }) => {
  return Joi.object({
    login: Joi.string()
      .required()
      .description(
        `Target user login to fetch the GitHub README.md as markdown.`
      ),
    branch: Joi.string().description(
      `Target branch to download the README.md defaults to [master] file and we do not recommend to change except in cases you are using the [main] branch.`
    ),
    filename: Joi.string().description(
      `Target filename, by default [README.md] and we do not recommend to change.`
    ),
    expression: Joi.string().description(
      `Raw GitHub query expression, in summary it is a Git revision expression suitable for rev-parse. See details of the Object [expression] argument at https://docs.github.com/en/graphql/reference/objects.`
    ),
  });
};
