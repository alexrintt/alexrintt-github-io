exports.pluginOptionsSchema = ({ Joi }) => {
  return Joi.object({
    queries: Joi.array()
      .items(Joi.string())
      .required()
      .description(
        `Set of queries that should be done when searching for repositories. This will fetch all results or `
      ),
    queryLimit: Joi.number().description(
      `Limit the results of each query separately. If you want to limit globally, use [resultsLimit] option instead.`
    ),
    resulsLimit: Joi.number().description(
      `Result entries limit, all pending queries (if any) will be aborted if this global limit has already reached.`
    ),
  });
};
