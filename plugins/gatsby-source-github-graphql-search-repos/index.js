module.exports.sourceNodes = async (
  {
    githubSourcePlugin: { graphql, githubPlainResolverFields, pluginNodeTypes },
  },
  pluginOptions
) => {
  pluginOptions = pluginOptions ?? {};

  const {
    login,
    branch = `master`,
    expression: rawExpression,
    filename = `README.md`,
  } = pluginOptions;

  const expression = rawExpression ?? `${branch}:${filename}`;

  const {
    user,
    repository: {
      object: { text },
    },
  } = await graphql(
    `
      query GetUserProfileReadme(
        $owner: String!
        $repo: String!
        $expression: String!
      ) {
        user(login: $owner) {
          ${githubPlainResolverFields.USER}
        }
        repository(owner: $owner, name: $repo) {
          object(expression: $expression) {
            ... on Blob {
              text
            }
          }
        }
      }
    `,
    { expression, owner: login, repo: login }
  );

  return {
    [pluginNodeTypes.USER]: [{ ...user, profileReadme: text }],
  };
};

module.exports.onCreateNode = async (
  {
    node,
    actions: { createNode },
    createContentDigest,
    createNodeId,
    githubSourcePlugin: { pluginNodeTypes },
  },
  pluginOptions
) => {
  if (node.internal.type === pluginNodeTypes.USER) {
    if (typeof node.profileReadme !== `string`) return;

    const content = node.profileReadme;

    await createNode({
      id: createNodeId(`${node.id} >>> ${content}`),
      parent: null,
      userId: node.id,
      children: [],
      internal: {
        type: `CustomPluginGitHubUserReadme`,
        mediaType: `text/markdown`,
        content: content,
        contentDigest: createContentDigest(content),
      },
    });
  }
};

module.exports.createSchemaCustomization = async (
  { actions: { createTypes }, githubSourcePlugin: { pluginNodeTypes } },
  pluginOptions
) => {
  const typedefs = `
    type ${pluginNodeTypes.USER} implements Node {
      profileReadme: CustomPluginGitHubUserReadme @link(from: "id", by: "userId")
    }
  `;
  createTypes(typedefs);
};
