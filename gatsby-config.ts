import type { GatsbyConfig } from "gatsby";
import blogConfig from "./blog.config";
import { slugify } from "./src/utils/slugify";
import parse from "html-dom-parser";
import type { Element } from "domhandler";

require(`dotenv`).config({
  path: `.env`,
});

function mapDiscussions(discussion: any) {
  // type of discussion == "Discussion", see GitHub API reference below.
  // https://docs.github.com/en/graphql/guides/using-the-graphql-api-for-discussions#discussion
  // Not all fields are included but most of them.

  const slug = slugify(discussion.title, discussion.createdAt);
  const path = (blogConfig.postsBasePath ?? ``) + `/` + slug;
  const url = blogConfig.domain + path;

  function getThumbnailUrlIfAny(string: string): string | undefined {
    const source = string.trim();
    const isMarkImg = source.startsWith(`![`);
    const isHtmlImg = source.startsWith(`<img`);

    if (!isMarkImg && !isHtmlImg) {
      return undefined;
    }

    function htmlImgFromMarkImg(source: string): string {
      const regex = /!\[(.*?)]\((https?:\/\/\S+\.\w+)\)/g;

      // Dangerous but we trust the HTML source since only users with repo write access can publish posts.
      return source.replace(regex, `<img alt="$1" src="$2" />`);
    }

    const imgString = isMarkImg ? htmlImgFromMarkImg(source) : source;
    const imgNode = parse(imgString)[0] as Element;
    const src = imgNode.attribs.src;

    if (!src.startsWith(`http`)) {
      return undefined;
    }

    return src;
  }

  const postLines = discussion.body.split(`\n`);

  const thumbnailImage = getThumbnailUrlIfAny(postLines[0]);
  const hasThumbnailImage = typeof thumbnailImage === `string`;

  return {
    ...discussion,
    discussionUrl: discussion.url,
    body: hasThumbnailImage ? postLines.slice(1).join(`\n`) : discussion.body,
    thumbnailImage: hasThumbnailImage ? thumbnailImage : null,
    hasThumbnailImage,
    url,
    path,
    slug,
  };
}

const packageJson = require(`./package.json`);

let config: GatsbyConfig = {
  siteMetadata: {
    title: `Alex Rintt - ${packageJson.description}.`,
    description: packageJson.description,
    twitterUsername: `@alexrinttt`,
    image: `/images/opengraphblogcard.png`,
    siteUrl:
      process.env.NODE_ENV === `development`
        ? `http://127.0.0.1:8000`
        : blogConfig.domain,
    blogConfig: blogConfig,
  },
  graphqlTypegen: true,
  plugins: [
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        postCssPlugins: [
          require(`tailwindcss`),
          require(`./tailwind.config.js`), // optional
        ],
      },
    },
    {
      resolve: `gatsby-source-github-graphql`,
      options: {
        // This plugin uses the GitHub GraphQL API to query data
        // and this API requires authentication.
        token: process.env.GITHUB_TOKEN,

        // In this project we are going to check if the first line of the discussion body
        // is a markdown (or html) image, if it is then set the [thumbnailImage] field with it.
        // To make it possible we need to map the responsive at build-time (through sourceNodes Gatsby API).
        createCustomMapper: ({
          githubSourcePlugin: { pluginNodeTypes },
        }: any) => {
          return {
            [pluginNodeTypes.DISCUSSION]: mapDiscussions,
          };
        },

        // To optimize the custom [thumbnailImage] field created in the mapping step, that is,
        // create a responsive image, we need to create the respective [File] node from the image URL.
        // In order to do that, we can use this helper function provided by the plugin:
        onCreateNode: async (
          {
            node,
            isInternalType,
            githubSourcePlugin: { pluginNodeTypes, createFileNodeFrom },
          }: any,
          pluginOptions: any
        ) => {
          if (node.internal.type === pluginNodeTypes.DISCUSSION) {
            await createFileNodeFrom({
              node,
              key: `thumbnailImage`,
              fieldName: `thumbnailImageFile`,
            });
          }
        },

        // The last step is to make define the custom field in the schema.
        createSchemaCustomization: (
          {
            actions: { createTypes },
            githubSourcePlugin: { pluginNodeTypes },
          }: any,
          pluginOptions: any
        ) => {
          // Always use [pluginNodeTypes] since you can also customize these Node types
          // if it is conflicting with another plugin.
          const typedef = `
            type ${pluginNodeTypes.DISCUSSION} implements Node {
              thumbnailImage: File @link(from: "fields.thumbnailImageFile")
            }
          `;

          createTypes(typedef);
        },
        plugins: [
          ...(process.env.NODE_ENV === `development`
            ? blogConfig.developmentCmsRepositories
            : blogConfig.cmsRepositories
          ).map((e) => {
            return {
              resolve: `gatsby-source-github-graphql-discussions`,
              options: {
                // Remember you can always create a second instance of
                // this plugin and assign a different `source` option
                // to fetch from discussion from multiple repositories.
                owner: e.owner,
                repo: e.name,

                // You can use this key to filter any resource.
                // So you can use multiple instances of this plugin and keep the relationships.
                source: `${e.owner}:${e.name}`,

                // Likely to be a Announcement type discussion category since only
                // users with repo write access can create discussions with this category
                // This allow a moderation when newcomers post something.
                categorySlugs: blogConfig.discussionCategories,
              },
            };
          }),
          {
            resolve: `gatsby-source-github-graphql-profile-readme`,
            options: {
              login: blogConfig.owner,
            },
          },
          {
            resolve: `gatsby-source-github-graphql-user-repos`,
            options: {
              login: blogConfig.owner,
              affiliations: [`OWNER`],
              ownerAffiliations: [`OWNER`],
              privacy: `PUBLIC`,
              // limit: 5,
            },
          },
        ],
      },
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `static/images/icon.png`,
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `@raae/gatsby-remark-oembed`,
          {
            resolve: `gatsby-remark-images-remote`,
            options: {
              staticDir: blogConfig.postsBasePath + `/static`,
              loading: `lazy`,
              backgroundColor: `#000000`,
              linkImagesToOriginal: true,
              sharpMethod: `fluid`,
              maxWidth: 920,
              quality: 50,
            },
          },
          `gatsby-remark-responsive-iframe`,
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              showLineNumbers: true,
              noInlineHighlight: false,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `./src/pages/`,
      },
      __key: `pages`,
    },
  ],
};

if (typeof blogConfig.disqusShortname === `string`) {
  config = {
    ...config,
    plugins: [
      ...config.plugins!,
      {
        resolve: `gatsby-plugin-disqus`,
        options: {
          shortname: blogConfig.disqusShortname,
        },
      },
    ],
  };
}

export default config;
