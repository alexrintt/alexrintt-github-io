import React from "react";

export const onRenderBody = ({ setHeadComponents }) => {
  setHeadComponents([
    <link
      rel="preload"
      href="/fonts/CascadiaMono.ttf"
      as="font"
      type="font/ttf"
      crossOrigin="anonymous"
      key="Cascadia Mono"
    />,
    <link
      rel="preload"
      href="/fonts/SourceSerifPro-Regular.ttf"
      as="font"
      type="font/ttf"
      crossOrigin="anonymous"
      key="Source Serif Pro"
    />,
  ]);
};
