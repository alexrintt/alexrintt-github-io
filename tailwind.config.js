/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,jsx,ts,tsx}",
    "./src/components/**/*.{js,jsx,ts,tsx}",
    "./src/templates/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      flex: {
        2: "2 2 0%",
        3: "3 3 0%",
        4: "4 4 0%",
      },
      maxWidth: {
        fhd: "1920px",
        "2k": "2048px",
      },
      maxHeight: {
        half: "50%",
      },
      colors: {
        s1: "var(--surface-1)",
        s2: "var(--surface-2)",
        s3: "var(--surface-3)",
        s4: "var(--surface-4)",
        p1: "var(--text-primary)",
        p2: "var(--text-secondary)",
        link: "var(--text-link)",
        lb: "var(--text-link-background)",
        ly: "var(--text-link-yellow)",
      },
      scale: {
        101: "1.01",
      },
    },
    fontFamily: {
      sans: ["var(--font-family)", "sans-serif"],
      serif: ["var(--font-family)", "serif"],
      code: ["var(--font-family-code)", "monospace"],
      variant: ["var(--font-family-variant)", "serif"],
    },
  },
  plugins: [],
};
